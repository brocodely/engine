@extends('layouts.main')
@section('title', 'Submenu Management')
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Title-->
					<h3 class="text-dark fw-bolder my-1">Submenu</h3>
					<!--end::Title-->
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item">
							<a href="{{ url('submenu') }}" class="text-muted text-hover-primary">Submenu</a>
						</li>
						<li class="breadcrumb-item text-dark">Edit</li>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<form action="{{ url('submenu/'. $submenu->id) }}" method="post" class="form-group">
							@csrf
							@method('patch')
							<div class="form-group mb-5">
								<label for="menu_id">Menu</label>
								<select class="form-select form-select-lg" id="menu_id" data-control="select2" data-placeholder="Pilih Menu" data-allow-clear="true" name="menu_id">
									<option value="{{ $submenu->menu_id }}">{{ $submenu->menu->name }}</option>
									@foreach ($menu as $mn)
										<option value="{{ $mn->id }}">{{ $mn->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group mb-5">
								<label for="name">Submenu</label>
								<input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Masukan Submenu" name="name" value="{{ $submenu->name }}">
								<small id="name" class="form-text text-muted">Gunakan submenu yang spesifik dan membantu</small>
							</div>
							<div class="form-group mb-5">
								<label for="url">URL</label>
								<input type="text" class="form-control" id="url" aria-describedby="url" placeholder="Masukan Submenu" name="url" value="{{ $submenu->url }}">
								<small id="url" class="form-text text-muted">Gunakan url yang benar</small>
							</div>
							<div class="form-group mb-5">
								<label for="is_active">Status</label>
								<select class="form-select form-select-lg" id="is_active" data-control="select2" data-placeholder="Pilih Status" data-allow-clear="true" name="is_active">
									<option value="{{ $submenu->is_active }}">{{ $submenu->is_active == '1' ? 'Aktif' : 'Tidak Aktif'}}</option>
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
							</div>
							
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn btn-warning" onclick="history.back()">Back</button>
						</form>
					</div>
					</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
<!--end::Main-->
@endsection