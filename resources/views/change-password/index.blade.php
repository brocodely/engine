@extends('layouts.main')
@section('title', 'Change Password')
@section('content')
	<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('profile/change-password') }}" class="text-muted text-hover-primary">Password</a>
						</li>
						<li class="breadcrumb-item text-dark">Change Password</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
                        <!--begin::Alert-->
                        @if (session('status'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="btn-close fs-6" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @elseif(session('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ session('error') }}
                                <button type="button" class="btn-close fs-6" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @endif
                        <!--end::Alert-->
                        
                        <h1>{{ $user->name }}</h1>
                        <div class="separator border-dark my-lg-10 my-5"></div>
                        <h3 class="mb-3">Change Password</h3>
                        <form action="{{ route('profile.change-password', $user->id) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="row">
                                <div class="col-lg-4 mb-5">
                                    <div class="form-group">
                                        <label for="current-password" class="required form-label">Password Sekarang</label>
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="current-password" aria-describedby="current-password" placeholder="Masukan password" name="password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        @if(!session('error'))
                                            <b><small class="text-small text-danger password"></small></b>
                                            <br>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="col-lg-4 mb-5">
                                    <div class="form-group">
                                        <label for="password" class="required form-label">Password Baru</label>
                                        <input type="password" class="form-control @error('password_baru') is-invalid @enderror" id="password" aria-describedby="password" placeholder="Masukan password" name="password_baru">
                                        @error('password_baru')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        @if(!session('error'))
                                            <b><small class="text-small text-danger password-baru"></small></b>
                                            <br>
                                        @endif

                                        <small id="password" class="form-text @error('password_baru') text-danger @enderror d-block">Panjang minimal 4 karakter</small>
                                        <small id="password" class="form-text @error('password_baru') text-danger @enderror">Panjang maksimal 20 karakter</small>
                                        <p id="password" class="form-text @error('password_baru') text-danger @enderror mt-0">Menggunakan campuran huruf dan angka</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="password_confirmation" class="required form-label">Konfirmasi Password Baru</label>
                                        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" aria-describedby="password_confirmation" placeholder="Konfirmasi password" name="password_confirmation">
                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        @if(!session('error'))
                                            <b><small class="text-small text-danger password-confirmation"></small></b>
                                            <br>
                                        @endif
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-light-dark mt-3">Reset</button>
                        </form>
					</div>
				</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
        });

        $('.form-control').on('keyup', function(){
            var password = $('input[name=password]').val();
            var password_baru = $('input[name=password_baru]').val();
            var password_confirmation = $('input[name=password_confirmation]').val();

            $.ajax({
                url: `{{ route('api.changeProccess') }}`,
                data: {password: password, password_baru:password_baru, password_confirmation:password_confirmation},
                success: function(response) {

                    console.log(response.error);

                    var passwordData = (response.error != undefined) && (response.error.password) ? response.error.password[0] : '';
                    var passwordBaruData = (response.error != undefined) && (response.error.password_baru) ? response.error.password_baru[0] : '';
                    var passwordConfirmationData = (response.error != undefined) && (response.error.password_confirmation) ? response.error.password_confirmation[0] : '';

                    if(passwordData != ''){
                        $('input[name=password]').addClass('is-invalid');
                    }else{
                        $('input[name=password]').removeClass('is-invalid');
                    }

                    if(passwordBaruData != ''){
                        $('input[name=password_baru]').addClass('is-invalid');
                    }else{
                        $('input[name=password_baru]').removeClass('is-invalid');
                    }

                    if(passwordConfirmationData != ''){
                        $('input[name=password_confirmation]').addClass('is-invalid');
                    }else{
                        $('input[name=password_confirmation]').removeClass('is-invalid');
                    }

                    $('.password').text(passwordData)
                    $('.password-baru').text(passwordBaruData)
                    $('.password-confirmation').text(passwordConfirmationData)

                }
            });

        });

    </script>
@endsection