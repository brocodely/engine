@extends('layouts.main')
@section('title', 'Menu Management')
@section('content')
	<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('/menu') }}" class="text-muted text-hover-primary">Menu</a>
						</li>
						<li class="breadcrumb-item text-dark">Menu Management</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<!--begin::Alert-->                                        
					
					<!--end::Alert-->
						<div class="table-responsive">
							<a href="{{ url('/menu/create') }}" class="btn btn-primary mb-3" >Tambah Menu Baru</a>
							@if (session('status'))
								<div class="alert alert-primary">
									{{ session('status') }}
								</div>
							@endif
							<table class="table table-striped table-hover gy-7 gs-7 text-center">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										<th>Menu</th>
										<th>Aksi</th>
										
									</tr>
								</thead>
								<tbody>                                                 
									@foreach($menu as $mn)
										<tr>
											<td>{{ $loop->iteration }}</td>
											<td>{{ $mn->name }}</td>
											<td>
												<form action="{{ url('menu'. '/' .$mn->id) }}" class="form-group" method="post">
													@csrf
													@method('delete')
													<a href="{{ 'menu/'. $mn->id . '/edit' }}" class="badge badge-primary">Edit</a>
													<button type="submit" class="badge badge-danger" onclick="return confirm('Are you sure?')" style="border:none">Delete</button>
												</form>
											</td>          
										</tr>       
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection