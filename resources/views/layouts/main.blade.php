<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>@yield('title') | {{ config('app.name') }}</title>
		<meta name="description" content="{{ config('app.name') }}" />
		<meta name="keywords" content="{{ config('app.name') }}" />
		<link rel="canonical" href="Https://preview.keenthemes.com/start" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="shortcut icon" href="{{ URL::asset('template/theme/dist') }}/assets/media/logos/kpu.svg" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->

		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="{{ URL::asset('assets/plugins/custom/leaflet') }}/leaflet.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->

		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{ URL::asset('assets/plugins/global') }}/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ URL::asset('assets/css') }}/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->

		<!-- My Style -->
		<link rel="stylesheet" href="{{ URL::asset('assets/css') }}/style.css">
		
        <!-- begin::ChartJS -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <!-- end::ChartJS -->
	</head>
	<!--end::Head-->

	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed aside-primary-disabled aside-secondary-enabled">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<div id="kt_aside" class="aside bg-info" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_toggler">
					<!--begin::Secondary-->
					<div class="aside-secondary d-flex flex-row-fluid bg-white">
						<!--begin::Workspace-->
						@include('components/sidebar')
						<!--end::Workspace-->
					</div>
					<!--end::Secondary-->
				</div>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					@include('components/topbar')
					<!--end::Header-->
					<!--begin::Main-->
					<div class="d-flex flex-column flex-column-fluid">
						<!--begin::Content-->
						@yield('content')
						<!--end::Content-->
					</div>
					<!--end::Main-->
					<!--begin::Footer-->
					@include('components/footer')
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		@include('components/misc')
		<!--end::Root-->
		<!--end::Main-->

		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="{{ URL::asset('assets/plugins/global') }}/plugins.bundle.js"></script>
		<script src="{{ URL::asset('assets/js') }}/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="{{ URL::asset('assets/plugins/custom/leaflet') }}/leaflet.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="{{ URL::asset('assets/js/custom') }}/widgets.js"></script>
		<script src="{{ URL::asset('assets/js/custom') }}/modals/create-app.js"></script>
		<script src="{{ URL::asset('assets/js/custom') }}/modals/select-location.js"></script>
		<script src="{{ URL::asset('assets/js/custom') }}/modals/users-search.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
		
		{{-- includeable scripts --}}

		{{-- all pages JS --}}

		<!--My Script-->
		<script src="{{ URL::asset('assets/js/script.js') }}"></script>

		<script>

			$(document).ready(function() {

				$(".menu-search").hide();

				$(".search-form").on("keyup", function() {
					var value = $(this).val().toLowerCase();
					
					$(".menu-search").show();
					$(".menu-search div").filter(function() {
						$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
					});

					if(value == ""){
						$(".menu-search").hide();
					}
				});

			});

			$('.btn-signout').on('click', function(event) {
				event.preventDefault();

				var form = $(this).closest('form');

				Swal.fire({
					title: 'Are you sure?',
					text: "You will be signed out!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, sign out!'
				}).then((result) => {
					if (result.isConfirmed) {
						form.submit();
					}
				});
			});

		</script>
		@yield('scripts')
	</body>
	<!--end::Body-->
</html>