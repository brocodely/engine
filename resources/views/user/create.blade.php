@extends('layouts.main')
@section('title', 'User Management')
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Title-->
					<h3 class="text-dark fw-bolder my-1">User</h3>
					<!--end::Title-->
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item">
							<a href="{{ url('/user') }}" class="text-muted text-hover-primary">User</a>
						</li>
						<li class="breadcrumb-item text-dark">Create</li>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						@if (session('failed'))
								<div class="alert alert-danger">
									{{ session('failed') }}
								</div>
							@endif
						<form action="{{ route('user.store') }}" method="post" class="form-group">
							@csrf
							<div class="form-group mb-5">
								<label for="role_id">Role</label>
								<select class="form-select form-select-lg" id="role_id" data-control="select2" data-placeholder="Pilih Role" data-allow-clear="true" name="role_id">
									<option></option>
									@foreach ($role as $rl)
										<option value="{{ $rl->id }}">{{ $rl->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group mb-5">
								<label for="name">Nama User</label>
								<input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Masukan Nama User" name="name">
								<small id="name" class="form-text text-muted">Gunakan nama user yang spesifik dan membantu</small>
							</div>
							<div class="form-group mb-5">
								<label for="image">Image</label>
								<input type="text" class="form-control" id="image" aria-describedby="image" name="image" value="default.svg">
							</div>
							<div class="form-group mb-5">
								<label for="email">Email</label>
								<input type="email" class="form-control" id="email" aria-describedby="email" name="email" placeholder="Masukan Email Anda">
							</div>
							<div class="form-group mb-5">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" aria-describedby="password" name="password" placeholder="Masukan Password Anda">
							</div>
							<div class="form-group mb-5">
								<label for="password2">Confirm Password</label>
								<input type="password" class="form-control" id="password2" aria-describedby="password2" name="password2" placeholder="Confirm Password">
							</div>
							
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn btn-warning" onclick="history.back()">Back</button>
						</form>
					</div>
					</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
<!--end::Main-->
@endsection