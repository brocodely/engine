$('#sidebar_toggle').on('click', function () {
    $('#kt_wrapper').toggleClass('open');
    $('#kt_aside').toggleClass('close');
});

function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('#image-profile');
    const textPreview = $('#text-preview');
    const deleteProfileButton = $('.delete-profile');

    const oFileReader = new FileReader();
    oFileReader.readAsDataURL(image.files[0]);

    oFileReader.onload = function (oFREvent) {
        imgPreview.src = oFREvent.target.result;
    }

    textPreview.removeClass('d-none');
    textPreview.addClass('d-block');

    deleteProfileButton.addClass('d-none');
}